const secrets = {
  dbUri: process.env.DB_URI || 'mongodb+srv://intern:TceMc@3541@cluster0-dzvcy.mongodb.net/medpartner',
};

const getSecret = (key) => secrets[key];

module.exports = { getSecret };