const express = require('express');
const bcrypt = require('bcryptjs');
const User = require('../models/user');
const router = express.Router();
var jwt = require('jsonwebtoken');
var config = require('../config');
var nodemailer = require('nodemailer');
var sgTransport = require('nodemailer-sendgrid-transport'); 
var helper = require('sendgrid').mail;
const async = require('async');
const isEmail = (email) => {
  if (typeof email !== 'string') {
    return false;
  }
  const emailRegex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
  return emailRegex.test(email);
};
var options = {
  auth: {
      api_user: 'intern2020', // Sendgrid username
      api_key: 'TceMc@3541' // Sendgrid password
  }
};
var client = nodemailer.createTransport({
 service: 'gmail',
 auth: {
     user: 'intern@tryangled.com', // Your email address
     pass: 'TceMc@3541' // Your password
 },
 tls: { rejectUnauthorized: false }
});
var client = nodemailer.createTransport(sgTransport(options));
router.post('/register',async(req, res) => {
  try {
    const {name,email,contact,dob,gender,password} = req.body;
    if (!isEmail(email)) {
      throw new Error('Email must be a valid email address.');
    }
    if (/^\d{10}$/.test(contact))
    {
      res.status(200);
    }
    else
    {
       throw new Error('invalid phone number');
    }
    const registereduser=await User.findOne({ email});
    const registerednumber=await User.findOne({ contact});
    if(registereduser)
    {
      throw new Error('User already exists');
    }
   
    else if(registerednumber)
    {
      
      throw new Error('User already exists');
    }
    const user = new User({ name,email,contact,dob,gender, password});
    const persistedUser = await user.save();
    res.status(201).json({
      title: 'User Registration Successful',
      detail: 'Successfully registered new user',
    });
  } catch (err) {
     res.status(400).json({
      errors: [
        {
          title: 'Registration Error',
          detail: 'Something went wrong during registration process.',
          errorMessage: err.message,
        },
      ],
    });
  }
});
router.post('/login', async (req, res) => {
    try {
      const { email, contact, password } = req.body;
      if (!isEmail(email)) {
        return res.status(400).json({
          errors: [
            {
              title: 'Bad Request',
              detail: 'Email must be a valid email address',
            },
          ],
        });
      }
      const user = await User.findOne({ email,contact });
      if (!user) {
        throw new Error();
      }
      const passwordValidated = await bcrypt.compare(password, user.password);
      if (!passwordValidated) {
        return res.status(401).send({ 
          message: 'please enter correct password',});
      }
     var token = jwt.sign({ id: user._id }, config.secret, {
        expiresIn: 86400 ,// expires in 24 hours
      });
      res.json({
        title: 'Login Successful',
        detail: 'Successfully validated user credentials',
        auth: true, token: token,
      });
    } catch (err) {
      res.status(401).json({
        errors: [
          {
            title: 'Invalid Credentials',
            detail: 'please enter correct email or phone number',
            errorMessage: 'Donot have an account please signup',
          },
        ],
      });
    }
  });
  router.put('/resetpassword', function(req, res) {
    User.findOne({ email: req.body.email }).select('username active email resettoken name').exec(function(err, user) {
      if (!user) {
        res.json({ success: false, message: 'Username was not found' }); // Return error if username is not found in database
      }
      else {
        user.resettoken = jwt.sign({ email: user.email }, config.secret, { expiresIn: '1h' }); // Create a token for activating account through e-mail
        // Save token to user in database
        user.save(function(err) {
            if (err) {
                res.json({ success: false, message: err }); // Return error if cannot connect
            } else {
                // Create e-mail object to send to user
                var email1 = {
                    from: 'intern@tryangled.com',
                    to: user.email,
                    subject: 'Reset Password Request',
                    text: 'Hello ' + user.name + ', You recently request a password reset link. Please click on the link below to reset your password:<br><br><a href="http://www.herokutestapp3z24.com/reset/' + user.resettoken,
                    html: 'Hello<strong> ' + user.name + '</strong>,<br><br>You recently request a password reset link. Please click on the link below to reset your password:<br><br><a href="http://www.herokutestapp3z24.com/reset/' + user.resettoken + '">http://www.herokutestapp3z24.com/reset/</a>'
                };
                // Function to send e-mail to the user
                client.sendMail(email1, function(err, info) {
                    if (err) {
                        console.log(err); // If error with sending e-mail, log to console/terminal
                    } else {
                        console.log(info); // Log success message to console
                        console.log('sent to: ' + user.email); // Log e-mail 
                    }
                });
                res.json({ success: true,
                   message: 'Please check your e-mail for password reset link',
                   token:user.resettoken

               }); // Return success message
            }
        });
    }
  });
});
router.get('/resetpassword/:token', function(req, res) {
  User.findOne({ resettoken: req.params.token }).select().exec(function(err, user) {
          var token = req.params.token; // Save user's token from parameters to variable
          console.log(token);
          // Function to verify token
          jwt.verify(token, config.secret, function(err, decoded) {
              if (err) {
                  res.json({ success: false, message: 'Password link has expired' }); // Token has expired or is invalid
              } else {
                  //if (!user) {
                    //  res.json({ success: false, message: 'Password link has expired' }); // Token is valid but not no user has that token anymore
                  //} 
                  //else {
                      res.json({ success: true, user: user }); // Return user object to controller
                  }
              //}
          });
  });
});
router.put('/savepassword', function(req, res) {
  User.findOne({ email: req.body.email }).select('email name password resettoken').exec(function(err, user) {
        console.log(req.body.password);
          if (req.body.password === null || req.body.password === '') {
              res.json({ success: false, message: 'Password not provided' });
          } else {
              user.password = req.body.password; // Save user's new password to the user object
              console.log(user.password);
              user.resettoken = false; // Clear user's resettoken 
              // Save user's new data
              user.save(function(err) {
                  if (err) {
                      res.json({ success: false, message: err });
                  } else {
                      // Create e-mail object to send to user
                      var email = {
                          from: 'intern@tryangled.com',
                          to: user.email,
                          subject: 'Password Recently Reset',
                          text: 'Hello ' + user.name + ', This e-mail is to notify you that your password was recently reset at localhost.com',
                          html: 'Hello<strong> ' + user.name + '</strong>,<br><br>This e-mail is to notify you that your password was recently reset at localhost.com'
                      };
                      // Function to send e-mail to the user
                      client.sendMail(email, function(err, info) {
                          if (err) console.log(err); // If error with sending e-mail, log to console/terminal
                      });
                      res.json({ success: true, message: 'Password has been reset!' }); // Return success message
                  }
              });
          }
  });
});

  router.get('/logout', function(req, res) {
    res.status(200).send({ auth: false, token: null });
  });
  module.exports = router;