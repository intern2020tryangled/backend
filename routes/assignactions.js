const express = require('express');
const Assignaction = require('../models/assignaction');
const router = express.Router();
const { check, validationResult } = require('express-validator');
router.post('/assignaction',async(req, res) => {
    try {
      const {type_of_booking} = req.body;
        const{patient_id,hospital,speciality,doctors_enqiry,country,state,preferred_embasy_name,date,time,flight_details,hotel_accomodations,
        test_name,center_name,location,note}=req.body;
        const assign = new Assignaction(
        {patient_id,type_of_booking,hospital,speciality,doctors_enqiry,country,state,preferred_embasy_name,date,time,flight_details,hotel_accomodations,
        test_name,center_name,location,note});
        const persistedUser =await assign.save();
      res.send({
        title: 'action assigned successfully',
        detail: 'Successfully addded user details',
   });
      
    }
    catch(err)
    {
        res.status(401).json({
            errors: [
              {
                title: 'data cannot be added',
                detail: 'please select any status...',
                errorMessage: err.message,
              },
            ],
          });
    }
});

module.exports = router;
