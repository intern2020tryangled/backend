const express = require('express');
const Patient= require('../models/patient');
const router = express.Router();
const jwt = require('jsonwebtoken');
var config = require('../config');
var nodemailer = require('nodemailer'); 
var db=require('../db.js');
var MongoClient = require('mongodb').MongoClient;
var sgTransport = require('nodemailer-sendgrid-transport'); 
var helper = require('sendgrid').mail;
const isEmail = (email) => {
    if (typeof email !== 'string') {
      return false;
    }
    const emailRegex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
    return emailRegex.test(email);
  };
  var options = {
    auth: {
        api_user: 'intern2020', // Sendgrid username
        api_key: 'TceMc@3541' // Sendgrid password
    }
};
var client = nodemailer.createTransport({
   service: 'gmail',
   auth: {
       user: 'intern@tryangled.com', // Your email address
       pass: 'TceMc@3541' // Your password
   },
   tls: { rejectUnauthorized: false }
});
var client = nodemailer.createTransport(sgTransport(options));
router.post('/add',async(req, res) => {
    try {
      const {name,gender,country,state,phone,email,address} = req.body;
      const registeredpatient=await Patient.findOne({ email});
   var token1 = req.header('token');
    //console.log(token1);
    if (!token1) return res.status(401).send({ auth: false, message: 'No token provided.' });
    jwt.verify(token1, config.secret, function(err, decoded) {
      if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
      else if(registeredpatient)
        {
          throw new Error('patient already exists');
        }
     else{
      if (!isEmail(email)) {
        throw new Error('Email must be a valid email address.');
      }
    if (/^\d{10}$/.test(phone)){
      res.status(200);
    }
    else
    {
      throw new Error('invalild phone number'); 
    }
      const patient = new Patient({ name,gender,country,state,phone,email,address});
       const persistedUser =patient.save();
      var email1 = {
        from: ' intern@tryangled.com',
        to: [patient.email],
        subject: 'Welcome to Med partner App',
        text: 'Hello ' + patient.name + ', your details added successfully',
        html: 'Hello<strong> ' + patient.name + '</strong>,<br><br><a>Welcome to med partner app... your details added successfully</a>'
      };
    client.sendMail(email1, function(err, info) {
      if (err) {
          console.log(err); // If error with sending e-mail, log to console/terminal
      } else {
          console.log(info); // Log success message to console if sent
          console.log(patient.email); // Display e-mail that it was sent to
      }
  });
  
}
});
  res.json({ success: true, message: 'Account registered! Please check your e-mail.',
      title: 'patient added Successful',
      detail: 'Successfully addded patient details',
  });
} catch (err) {
      res.status(401).json({
        errors: [
          {
            title: 'Invalid Credentials',
            detail: 'please enter correct details...',
            errorMessage: err.message,
          },
        ],
      });
    }
  });
  router.get('/viewpatient', function (req, res) {
    var token1 = req.header('token');
    console.log(token1);
    if (!token1) return res.status(401).send({ auth: false, message: 'No token provided.' });
    jwt.verify(token1, config.secret, function(err, decoded) {
      if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
      else
      {
      Patient.find({}, function (err, patients) {
        if (err) return res.status(500).send("There was a problem finding the users.");
        res.status(200).send(patients);
        if(!patients) return res.status(404).send("No user found");
    });
  }
  });
});
router.get('/viewpatient/:id', function (req, res) {
    Patient.findById(req.params.id, function (err, patients) {
        if (err) return res.status(500).send("There was a problem finding the patient.");
        if (!patients) return res.status(404).send("No user found.");
        res.status(200).send(patients);
    });
});
router.put('/viewpatient/:name', function (req, res) {
  Patient.findOne({name:req.params.name}).select('name').exec(function (err, patients) {
      if (err) return res.status(500).send("There was a problem finding the patient.");
      if (!patients) return res.status(404).send("No user found.");
      res.status(200).send(patients);
  });
});
router.post('/search',function(req,res){
  var url = "mongodb://localhost:27017/register";
  MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  //var dbo = db.db("sample");
  const{name,patient_id,hospital}=req.body;
  db.collection('patients').aggregate([
    //{ "$match": { "assignactions.hospital": "apollo" }, },
    {
      $lookup:
        {
          from: "assignactions",
          localField: "_id",
          foreignField: "patient_id",
          as: "patient_details"

        }
   }
   
]).toArray(function(err, res) {
   
    if (err) throw err;
   const resultdoc=JSON.stringify(res);
   console.log(resultdoc);
  db.close();
  });
});

});

module.exports = router;
        