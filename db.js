const mongoose  = require('mongoose');
mongoose.Promise = global.Promise;
ObjectId = require('mongodb').ObjectId
mongoose.connect( 'mongodb://localhost:27017/register',
    { 
      useNewUrlParser: true,
      useUnifiedTopology: true  
    })
  .then(
    () => {
      console.log('Connected to mongoDB');
    },
    (err) => console.log('Error connecting to mongoDB', err)
  );