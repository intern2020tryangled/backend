const { getSecret } = require('./secrets');
const express = require('express');
const app = express();
app.use(express.json());
const database=require('./db.js')
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const usersRoute = require('./routes/users');
const patientsRoute=require('./routes/patients');
const assignactionRoute=require('./routes/assignactions');
const port = process.env.PORT || 3000;
app.use(cookieParser());
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true, parameterLimit: 1000000}));
app.use('/api/users', usersRoute);
app.use('/api/patients', patientsRoute);
app.use('/api/type_of_booking',assignactionRoute);
app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
module.exports = { app };