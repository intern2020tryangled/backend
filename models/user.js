const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
const uniqueValidator = require('mongoose-unique-validator');
const bcrypt = require('bcryptjs');
var titlize = require('mongoose-title-case'); // Import Mongoose Title Case Plugin
var validate = require('mongoose-validator');

const UserSchema = new mongoose.Schema({
    name:{
        type:String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        minlength: 10,
        trim: true,
    },
    contact:{
        type: Number,
        required:true,
        minlength:10,
    },
    dob:{
        type:Date,
        required:true,
        minlength:10,
    },
    gender:{
        type:String,
        enum: ['male', 'female', 'not-specified'],
        required:true,
        default: 'not-specified',
    },
    password: {
        type: String,
        required: true,
    }
});

UserSchema.plugin(uniqueValidator);
UserSchema.pre('save', function(next) {
  let user = this;
  if (!user.isModified('password')) {
    return next();
  }
  bcrypt
  .genSalt(12)
  .then((salt) => {
    return bcrypt.hash(user.password, salt);
  })
  .then((hash) => {
    user.password = hash;
    next();
  })
});

module.exports = mongoose.model('User', UserSchema);