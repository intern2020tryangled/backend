const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
const uniqueValidator = require('mongoose-unique-validator');
var patients=require('./patient.js');
const bcrypt = require('bcryptjs');
var Schema=mongoose.Schema;
mongoose.Promise = global.Promise
const ObjectId = mongoose.ObjectId;
const AssignactionSchema = new mongoose.Schema({
    patient_id:[{
        type:Schema.Types.ObjectId,
        required:true,
        ref:'patients'
    }],
    type_of_booking:{
        type:Number,
        required:true
    },
    hospital:
    {
       type:String,
       enum : ['apollo', 'meenatchi'],
        required:false,
    },
    speciality:{
        type:String,
        required:false,
    },
    country:{
        type:String,
        required:false,
    },
    state:{
        type:String,
        required:false,
    },
    doctors_enqiry:{
        type:String,
        required:false,
    },
    flight_details:
    {
        type:String,
        required:false,
    },
    hotel_accomodations:
    {
        type:String,
        enum : ['YES', 'NO'],
        required:false,
    },
    preferred_embasy_name:{
        type:String,
        required:false,
    },
    date:
    {
        type:Date,
        required:false,
    },
    time:
    {
        type:Number,
        required:false,
    },
    
    test_name:
    {
        type:String,
        required:false,
    },
    center_name:
    {
        type:String,
        required:false,
    },
    location:
    {
        type:String,
        required:false,
    },
    note:
    {
        type:String,
        required:false,
    }
    
});
AssignactionSchema.plugin(uniqueValidator);
module.exports = mongoose.model('Assignaction', AssignactionSchema);