const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
const uniqueValidator = require('mongoose-unique-validator');
const
 PatientSchema = new mongoose.Schema({
    name:
    {
        type:String,
        required: true,
    },
    gender:
    {
        type:String,
        required: true,
    },
    country:
    {
        type:String,
        required:true,
    },
    state:
    {
        type:String,
        required:true,
    },
    phone:
    {
        type: Number,
        required:true,
        minlength:10,
        unique:true,
    },
    email:
    {   
        type: String,
        required: true,
        minlength: 5,
        trim: true,
        unique: true,
    },
    address:
    {   
        type: String,
        required:true,
    },
});
    PatientSchema.plugin(uniqueValidator);
    module.exports = mongoose.model('Patient', PatientSchema);